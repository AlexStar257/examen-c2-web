const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

let datos = [
	{
		matricula: "2020030301",
		nombre: "Margarita Lizarraga",
		sexo: "M",
		materias: ["Ingles", "Bse de datos", "Tecnologias I"],
	},
	{
		matricula: "2020030314",
		nombre: "Alejandro Trejo",
		sexo: "H",
		materias: ["Ingles", "Bse de datos", "Tecnologias I"],
	},
	{
		matricula: "2020030101",
		nombre: "Ariana Trejo",
		sexo: "M",
		materias: ["Ingles", "Base de datos", "Tecnologias I"],
	},
];


// Cotización

router.get("/recursoshumanos", (req, res) => {
	const params = {
		numContrato: req.body.numContrato,
		domicilio: req.body.domicilio,
		nombre: req.body.nombre,
		pagoDiario: req.body.pagoDiario,
		diasTrabajados: req.body.diasTrabajados,
		nivel: req.body.nivel,
	};
	res.render("recursoshumanos.html", params);
});

router.post("/recursoshumanos", (req, res) => {
	const params = {
		numContrato: req.body.numContrato,
		domicilio: req.body.domicilio,
		nombre: req.body.nombre,
		pagoDiario: req.body.pagoDiario,
		diasTrabajados: req.body.diasTrabajados,
		nivel: req.body.nivel,
	};
	res.render("recursoshumanos.html", params);
});

router.get("/paginasalida", (req, res) => {
	const params = {
		numContrato: req.body.numContrato,
		domicilio: req.body.domicilio,
		nombre: req.body.nombre,
		pagoDiario: req.body.pagoDiario,
		diasTrabajados: req.body.diasTrabajados,
		nivel: req.body.nivel,
	};
	res.render("paginasalida.html", params);
});

router.post("/paginasalida", (req, res) => {
	const params = {
		numContrato: req.body.numContrato,
		domicilio: req.body.domicilio,
		nombre: req.body.nombre,
		pagoDiario: req.body.pagoDiario,
		diasTrabajados: req.body.diasTrabajados,
		nivel: req.body.nivel,
	};
	res.render("paginasalida.html", params);
});



//LA PAGINA ERROR VA AL FINAL DE GET/ POST

router.get("/", (req, res) => {
	res.render("index.html", { titulo: "Actividad PreExamen", nombre: "Alejandro Lizarraga Motta", grupo: "8-3", listado: datos });
});

module.exports = router;
