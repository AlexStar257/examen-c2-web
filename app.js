const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const misRutas = require("./router/index.js");
const path = require("path");
const app = express();

app.set("view engine", "ejs");
//tomar valor estatico : carpet ay se agrega public
app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({ extended: true }));

app.engine("html", require("ejs").renderFile);

app.use(misRutas);

//     ARREGLO DE OBJETOS CON REGISTROS

app.use((req, res, next) => {
	res.status(404).sendFile(__dirname + "/views/404error.html");
});

const puerto = 3000;
app.listen(puerto, () => {
	console.log("Iniciando puerto");
});
